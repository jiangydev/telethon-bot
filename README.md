# A bot for Starcoin

```shell script
git clone https://jiangydev@bitbucket.org/jiangydev/telethon-bot.git
```

```shell script
docker build -t jiangydev/telethon-bot:1.0.0 .

# dev
docker run -dit --restart=always \
    -v $(pwd)/config-dev.py:/usr/src/app/conf/config.py \
    -v $(pwd)/dev-db/:/usr/src/app/db/ \
    jiangydev/telethon-bot:dev

# prod
docker run -dit --restart=always \
    -v $(pwd)/config-prod.py:/usr/src/app/conf/config.py \
    -v $(pwd)/prod-db/:/usr/src/app/db/ \
    jiangydev/telethon-bot:1.0.0
```