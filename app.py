import logging

from telethon import TelegramClient, events, Button
from telethon.network import connection

from conf.config import configuration
from util.db_util import SqliteDB

logging.basicConfig(format='[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s',
                    level=logging.INFO)

use_proxy = False

if configuration['proxy'] is not None and configuration['proxy']['enabled']:
    use_proxy = True

if use_proxy:
    proxy_custom = (configuration['proxy']['host'], configuration['proxy']['port'], configuration['proxy']['secret'])
    bot = TelegramClient(configuration['bot']['username'], configuration['api']['id'], configuration['api']['hash']
                         , connection=connection.ConnectionTcpMTProxyIntermediate
                         , proxy=proxy_custom
                         ).start(bot_token=configuration['bot']['token'])
else:
    bot = TelegramClient(configuration['bot']['username'], configuration['api']['id'], configuration['api']['hash']
                         ).start(bot_token=configuration['bot']['token'])

chats_list = []


@bot.on(events.NewMessage(pattern='/start'))
async def start(event):
    """
    Send a message when the command /start is issued.
    :param event:
    :return:
    """
    logging.info('监听到 /start 指令: %s', event)
    if event.is_group:
        me = await bot.get_me()
        if not event.message.message == '/start@' + me.username:
            return
        is_admin = await judge_admin(event)
        if is_admin:
            if event.chat_id in chats_list:
                await event.respond('Starcoin 小助手已启用。')
            else:
                with SqliteDB('./db/telethon-bot.db') as db:
                    query_disable_set = db.execute(
                        'select id from chat where chat_channel_id=? and enabled=? and is_delete=False',
                        (event.chat_id, False,)).fetchall()
                    if len(query_disable_set) == 0:
                        db.execute('insert into chat (chat_channel_id) values (?)', (event.chat_id,))
                    else:
                        db.execute('update chat set enabled=? where id=?', (True, query_disable_set[0][0],))
                chats_list.append(event.chat_id)
                await event.respond('欢迎使用 Starcoin 小助手。')
    else:
        await event.respond('Starcoin 小助手仅可在群组中使用，请把小助手添加到群组，并赋予管理员权限。')
        return


async def judge_admin(event):
    """
    判断操作的用户是不是管理员
    :param event:
    :return:
    """
    is_admin = False
    if event.sender_id is None:
        # 如果发送者ID是None也认为是管理员
        is_admin = True
    else:
        async for group_user in bot.iter_participants(event.chat_id):
            if group_user.id == event.sender_id and getattr(group_user.participant, 'admin_rights', None) is not None:
                is_admin = True
                break
    logging.info('判断是否管理员结果: %s', is_admin)
    return is_admin


@bot.on(events.NewMessage(pattern='/stop'))
async def stop(event):
    """
    Send a message when the command /stop is issued.
    :param event:
    :return:
    """
    logging.info('监听到 /stop 指令: %s', event)
    if event.is_group:
        me = await bot.get_me()
        if not event.message.message == '/stop@' + me.username:
            return
        is_admin = await judge_admin(event)
        if is_admin:
            with SqliteDB('./db/telethon-bot.db') as db:
                query_disable_set = db.execute(
                    'select id from chat where chat_channel_id=? and enabled=? and is_delete=False',
                    (event.chat_id, True,)).fetchall()
                if len(query_disable_set) != 0:
                    db.execute('update chat set enabled=? where id=?', (False, query_disable_set[0][0],))
                db.execute('update chat set enabled=? where chat_channel_id=?', (False, event.chat_id,))
            if event.chat_id in chats_list:
                chats_list.remove(event.chat_id)
                await event.respond('Starcoin 小助手已停用，感谢使用。')
    else:
        await event.respond('Starcoin 小助手仅可在群组中使用，请把小助手添加到群组，并赋予管理员权限。')
        return


@bot.on(events.NewMessage(chats=chats_list))
async def new_message_handler(event):
    logging.info('监听到新消息: %s', event)


@bot.on(events.ChatAction(chats=chats_list))
async def chat_action_handler(event):
    logging.info('监听到频道/群组事件: %s', event)
    if event.user_joined or event.user_added:
        username = build_user_mention(event.users)
        # markup = event.client.build_reply_markup(
        markup = [
            [Button.url('💫 Home', 'https://starcoin.org')],
            [Button.url('🐙 GitHub', 'https://github.com/starcoinorg'),
             Button.url('🖥 Development', 'https://developer.starcoin.org')],
            [Button.url('🔍 Explorer', 'https://stcscan.io'),
             Button.url('💰️ Wallet(StarMask)',
                        'https://chrome.google.com/webstore/detail/starmask/mfhbebgoclkghebffdldpobeajmbecfk')],
            [Button.url('🕊 Twitter', 'https://twitter.com/StarcoinSTC'),
             Button.url('🤖 Discord', 'https://discord.com/invite/qFJTGKM7vK')]
        ]
        # )
        await event.respond(f"""
        {username}: \nHi~ Welcome to Join the Starcoin Telegram!\n\nStarcoin is a decentralized hierarchical smart contract network. It aims to provide a secured digital asset and a decentralized financial operation platform, so that the blockchain can be applied in more fields with a lower threshold.\n\nGet More 👇\n\n你好~ 欢迎加入 Starcoin 官方 TG 社区。\n\nStarcoin 是一个去中心化分层智能合约网络，它旨在提供一种安全的数字资产及去中心化金融运行平台，让区块链能够更低门槛应用在更多领域。\n\n了解更多 👇
        """, buttons=markup)
    if event.user_left or event.user_joined or event.user_added:
        logging.info('删除频道/群组消息')
        await bot.delete_messages(event.chat_id, message_ids=[event.action_message.id])


def build_user_mention(users):
    """
    构造被提及的用户
    :param users: 用户集合
    :return:
    """
    user_mention_list = []
    for user in users:
        username = user.username
        if username is None:
            username = user.first_name
        if username is None:
            username = user.last_name
        user_mention = f'[{username}](tg://user?id={user.id})'
        user_mention_list.append(user_mention)
    return list_to_str(user_mention_list, ' ')


def list_to_str(str_list, split=','):
    """
    字符集合转字符串
    :param str_list: 字符集合
    :param split:    分隔符
    :return: 字符串
    """
    result = ''
    for i in str_list:
        if result == '':
            result = i
        else:
            result = result + split + i
    return result


def init():
    # 创建数据库
    create_tb_cmd = '''
        CREATE TABLE IF NOT EXISTS chat
        (
        id                  INTEGER    PRIMARY KEY AUTOINCREMENT,         -- 自增主键
        chat_channel_id     INTEGER     NOT NULL default 0,                      -- 频道/群组ID
        welcome             TEXT       NOT NULL default '',                     -- 欢迎语
        enabled             BOOLEAN    NOT NULL default 1,                      -- 是否启用
        is_delete           BOOLEAN    NOT NULL default 0,                      -- 是否归档
        create_time         DATETIME   default (datetime('now', 'localtime'))   -- 创建时间
        );
        '''
    with SqliteDB('./db/telethon-bot.db') as db:
        db.execute(create_tb_cmd)
    # 从数据库中获取启用的频道/群组ID
    with SqliteDB('./db/telethon-bot.db') as db:
        query_chat_set = db.execute('select id,chat_channel_id from chat where enabled=? and is_delete=?',
                                    (True, False,)).fetchall()
        for item in query_chat_set:
            chat_channel_id = item[1]
            if chat_channel_id not in chats_list:
                chats_list.append(item[1])


if __name__ == '__main__':
    logging.info('开始初始化数据库...')
    init()
    logging.info('初始化数据库完成!')
    logging.info('开始启动机器人...')
    bot.run_until_disconnected()
