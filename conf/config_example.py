configuration = {
    'proxy': {
        'enabled': False,
        'host': '1.2.3.4',
        'port': 443,
        'secret': 'secret'
    },
    'api': {
        'id': 12345678,
        'hash': '1a2b3c4d5e6f'
    },
    'bot': {
        'token': '12345678:1a2b3c4d5e6f',
        'username': 'starcoin_official_bot'
    }
}